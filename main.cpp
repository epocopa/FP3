/*
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
// AUTORÍA: DANIEL FIDALGO PANERA y HÉCTOR FERNÁNDEZ SAN SOTERO
// https://gitlab.com/epocopa/FP3

#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>
#include <cstdlib>
#include "plataforma.h"

using namespace std;

const int MAX = 50;
const int MAXH = 10;
const int MAXE = 100;
const int PROD_NULO = 0;
const int CENTINELA = -1;

typedef enum {libre, muro, destinoL, destinoC, destinoJ, jugador, caja}tCasilla;
typedef enum {arriba, abajo, derecha, izquierda, salir, nada, deshacer }tTecla;

typedef tCasilla tTablero[MAX][MAX];

typedef struct{
  tTablero tablero;
  int nfilas;
  int ncolumnas;
  int filaJugador;
  int columJugador;
  int numCajas;
  int cajasOK;
}tSokoban;

typedef struct{
  tSokoban sokoban[MAXH];
  int cont;
}tHistoria;

typedef struct{
  tSokoban sokoban;
  int numMovimientos;
  string nFichero;
  int nivel;
  tHistoria historia;
  int deshechos = 0;
}tJuego;

typedef struct{
  int numMovimientos;
  string nFichero;
  int nivel;
}tPartida;

typedef tPartida tExitos[MAXE];

typedef struct{
  string nombre;
  tExitos exitos;
  int resueltas;
}tInfo;

void nombre(tInfo &info);
int menu();
void ejecutarOpc(int opc, tJuego &juego, tInfo &info);
void limpiar();
void pausa();
// Inicializa el tablero, haciendo que todas las
// MAX x MAX casillas estén libres y el número de movimientos a 0.
void inicializa(tJuego &juego);
bool cargarJuego(tJuego &juego);
bool cargarNivel(ifstream &fichero, tSokoban &sokoban, int nivel);
void dibujaCasilla(tCasilla casilla);
void dibujar(const tJuego &juego, bool fin);
tCasilla convertir(char letra);
tTecla leerTecla();
void hacerMovimiento(tJuego &juego, tTecla tecla);
bool movimientoValido(tJuego &juego, tCasilla casilla, tTecla tecla);
int menu();
bool bloqueado(const tJuego &juego);
bool deshacerMovimiento(tJuego &juego);
bool operator==(tPartida part1, tPartida part2);
bool operator<(tPartida part1, tPartida part2);
void guardarInfo(tPartida &partida, tInfo &info);

int main() {
  int opc;
  tJuego juego;
  tInfo info;
  juego.historia.cont = 0;
  nombre(info);
  while (opc = menu()) {
    ejecutarOpc(opc, juego, info);
  }

  return 0;
}

void nombre(tInfo &info){
  int i = 0;
  string ntemp;
  bool existe;
  cout << "Introduce tu nombre: ";
  cin >> info.nombre;
  limpiar();
  ifstream fich(info.nombre.c_str());
  if (fich.is_open()) {
    fich >> ntemp;
    while (!fich.eof()) {
      info.exitos[i].nFichero = ntemp;
      fich >> info.exitos[i].nivel;
      fich >> info.exitos[i].numMovimientos;
      fich >> ntemp;
      i++;
    }
    info.resueltas = i;
  }
}

int menu(){
  int op;
  do {
    cout << "\n           SOKOBAN\n";
    cout << "-------------------------------------------\n";
    cout << " 1 – Jugar partida\n";
    cout << " 2 – Informacion niveles\n";
    cout << " 3 – Ayuda\n";
    cout << " 0 - Salir\n";
    cout << "-------------------------------------------\n";
    cout << "Introduce una opcción: ";
    cin >> op;
  } while(op < 0 || op > 3);
  return op;
}

void ejecutarOpc(int opc, tJuego &juego, tInfo &info){
  bool fin = false;
  int cajasNo;
  tTecla tecla;
  switch (opc) {
    case 1:
      do {
        cout << "Introduce un nivel(1-5): ";
        cin >> juego.nivel;
      } while(juego.nivel < 1 || juego.nivel > 5);
      inicializa(juego);
      cargarJuego(juego);
      limpiar();
      dibujar(juego, fin);
      tecla = leerTecla();
      while (tecla != salir && !fin) {
        limpiar();
        if (tecla != deshacer) {
          hacerMovimiento(juego, tecla);
        } else{
          deshacerMovimiento(juego);
        }
        cajasNo = 0;
        for (int i = 0; i < MAX; i++) {
          for (int j = 0; j < MAX; j++) {
            if (juego.sokoban.tablero[i][j] == caja){
              cajasNo++;
            }
          }
        }

        if (cajasNo == 0) {
          fin = true;
        }
        dibujar(juego, fin);
        if (!fin) {
          tecla = leerTecla();
        }
        if (bloqueado(juego) && !fin) {
          while (tecla != deshacer) {
            cout << "BLOQUEADO\n";
            tecla = leerTecla();
            hacerMovimiento(juego, tecla);
          }
        }
      }
      if (fin) {
        tPartida partida;
        partida.numMovimientos = juego.numMovimientos;
			  partida.nivel = juego.nivel;
			  partida.nFichero = juego.nFichero;
        guardarInfo(partida, info);
      }
      break;
    case 2:
      limpiar();
      cout << "------------------------\n";
      for (int i = 0; i < info.resueltas; i++) {
        cout << info.exitos[i].nFichero << " " << info.exitos[i].nivel << " "
        << info.exitos[i].numMovimientos << "\n";
      }
      cout << "-------------------------\n";
      cout << "\nPulse una tecla para volver al menú pincipal.\n";
      pausa();
      _getch();
      limpiar();
      break;
    case 3:
      limpiar();
      cout << "\n  AYUDA SOKOBAN\n";
      cout << "------------------------\n";
      cout << " W – Mover arriba\n";
      cout << " A – Mover izquierda\n";
      cout << " S – Mover abajo\n";
      cout << " D - Mover derecha\n";
      cout << " R - Deshacer\n";
      cout << " ESC - Salir\n";
      cout << "-------------------------\n";
      cout << "\nPulse una tecla para volver al menú pincipal.\n";
      pausa();
      _getch();
      limpiar();
      break;
  }
}

void limpiar(){
#ifdef _WIN32
    system("cls");
#else
    system ("clear");
#endif
}

void pausa(){
#ifdef _WIN32
    system("pause");
#else
     _getch();
#endif
}

void inicializa(tJuego &juego){
  for (int i = 0; i < MAX; i++) {
    for (int j = 0; j < MAX; j++) {
      juego.sokoban.tablero[i][j] = libre;
    }
  }
  juego.numMovimientos = 0;
}


bool cargarJuego(tJuego &juego){
  bool cargado;
  juego.nFichero = "pruebas";
  ifstream fichero(juego.nFichero);
  if (fichero.is_open()) {
    cargarNivel(fichero, juego.sokoban, juego.nivel);
    cargado = true;
  } else{
    cargado = false;
  }
  return cargado;
}

bool cargarNivel(ifstream &fichero, tSokoban &sokoban, int nivel){
  //TODO: NO funciona en niveles irregulares a la derecha
  sokoban.cajasOK = 0;
  bool gamer = false;
  sokoban.ncolumnas = 0;
  sokoban.nfilas = -1;
  string s, linea;
  //Entero a string
  s = to_string(nivel);
  do {
    getline(fichero, linea);
  } while(linea != "Level " + s);
  cout <<linea << "\n\n";
  //estamos en linea debajo de level
  while (linea != "" && !fichero.eof()) {
    getline(fichero, linea);
    if (linea != "") {
      sokoban.nfilas++;
      if (sokoban.ncolumnas < linea.length()) {
        sokoban.ncolumnas = linea.length();
      }
      for (int i = 0; i < linea.length(); i++) {
        sokoban.tablero[sokoban.nfilas][i] = convertir(linea.at(i));
        if (convertir(linea.at(i)) == destinoC) {
          sokoban.cajasOK++;
        }
        if (!gamer && convertir(linea.at(i)) == jugador) {
          sokoban.filaJugador = sokoban.nfilas;
          sokoban.columJugador = i;
          gamer = false;
        }
      }
    }
  }
}

void dibujaCasilla(tCasilla casilla){
  #ifdef _WIN32
  	switch (casilla) {
  	case libre:
  		setColor(Azul);
  		cout << "-";
  		break;
  	case muro:
  		setColor(Verde);
  		cout << "|";
  		break;
  	case destinoL:
  		setColor(Rojo);
  		cout << ".";
  		break;
  	case destinoJ:
  		setColor(Azul);
  		cout << ":)";
  		break;
  	case destinoC:
  		setColor(Gris);
  		cout << "*";
  		break;
  	case jugador:
  		setColor(Blanco);
  		cout << "0";
  		break;
  	case caja:
  		setColor(Gris);
  		cout << "o";
  		break;
  	}
  #else
  switch (casilla) {
	case libre:
   configuraConsola(FondoAzul);
   cout << setw(2) << " ";
		break;
	case muro:
   configuraConsola(FondoVerde);
   cout << setw(2) << "  ";
		break;
	case destinoL:
    configuraConsola(BlancoSobreRojo);
    cout << setw(2) << "..";
		break;
	case destinoJ:
    configuraConsola(BlancoSobreAmarillo);
    cout << setw(2) << "\\/";
		break;
	case destinoC:
    configuraConsola(BlancoSobreAmarillo);
    cout << setw(2) << "[]";
		break;
	case jugador:
    configuraConsola(BlancoSobreNegro);
    cout << setw(2) << "00";
		break;
	case caja:
    configuraConsola(BlancoSobreAmarillo);
    cout << setw(2) << "()";
		break;
	}
  #endif
}

void dibujar(const tJuego &juego, bool fin){
  cout << "   SOKOBAN\n";
  int muros;
  for (int i = 0; i <= juego.sokoban.nfilas; i++) {
    muros = 0;
    for (int j = 0; j < juego.sokoban.ncolumnas; j++) {
      if (juego.sokoban.tablero[i][j] == muro) {
        muros++;
      }
      if (muros > 0) {
        dibujaCasilla(juego.sokoban.tablero[i][j]);
      } else {
        cout << setw(2) << " ";
      }
    }
    #ifdef _WIN32
			setColor(Gris);
		#else
			configuraConsola(Reset);
		#endif
    cout << "\n";
  }
  cout << juego.numMovimientos << " movimientos;   " << juego.deshechos <<
          "  deshechos\n";
  if (fin) {
    cout << "\n¡NIVEL COMPLETADO! ノ^_^)ノ┻━┻\n\n";
  }
}

tCasilla convertir(char letra){
  tCasilla x;
  switch (letra) {
    case '#':
      x = muro;
      break;
    case '@':
      x = jugador;
      break;
    case ' ':
      x = libre;
      break;
    case '$':
      x = caja;
      break;
    case '*':
      x = destinoC;
      break;
    case '+':
      x = destinoJ;
      break;
    case '.':
      x = destinoL;
      break;
    }
    return x;
}

tTecla leerTecla() {
	tTecla tecla;
	int dir;
	cin.sync();
  dir = _getch();
	switch (dir) {
		case 27: tecla = salir;
			break;
		case 119: tecla = arriba;
			break;
		case 115: tecla = abajo;
			break;
		case 100: tecla = derecha;
			break;
		case 97: tecla = izquierda;
			break;
		case 114: tecla = deshacer;
			break;
		default: tecla = nada;
			break;
	 }
  //cout << "Tecla: " << tecla << "\n";
	return tecla;
}

void hacerMovimiento(tJuego &juego, tTecla tecla) {
  bool moverJ = false;
  int filaDestino, columnaDestino,filaSiguiente, columnaSiguiente;
  tCasilla casilla, posUno, posDos;
  //Parte deshacer
  juego.historia.sokoban[juego.historia.cont] = juego.sokoban;
  if (juego.historia.cont < 9) {
    juego.historia.cont++;
  } else{
    for (int i = 1; i <= 9; i++) {
      juego.historia.sokoban[i-1] = juego.historia.sokoban[i];
    }
  }
    // Según lo explicado en clase para evitar un caos de ifs
  int col = 0, fil = 0;
  switch (tecla) {
case arriba:
    fil--;
    break;
  case abajo:
    fil++;
    break;
  case derecha:
    col++;
    break;
  case izquierda:
    col--;
    break;
  }
  filaDestino = juego.sokoban.filaJugador+fil;
  filaSiguiente = juego.sokoban.filaJugador+2*fil;
  columnaDestino = juego.sokoban.columJugador+col;
  columnaSiguiente = juego.sokoban.columJugador+2*col;
  //Asi es mas legible
  posUno = juego.sokoban.tablero[filaDestino][columnaDestino];
  posDos = juego.sokoban.tablero[filaSiguiente][columnaSiguiente];
  //Monton de ifs innecesarios por condiciones que en caso de que
  //se produzcan, no generan movimiento
  switch (posUno) {
  case libre:
        moverJ = true;
      break;
    case caja:
      if (posDos == libre) {
        juego.sokoban.tablero[filaSiguiente][columnaSiguiente] = caja;
        moverJ = true;
      } else if(posDos == destinoL){
        juego.sokoban.cajasOK++;
        juego.sokoban.tablero[filaSiguiente][columnaSiguiente] = destinoC;
        moverJ = true;
      }
      break;
    case muro:
      moverJ = false;
      break;
    case destinoL:
      moverJ = true;
      if (juego.sokoban.tablero[juego.sokoban.filaJugador]
          [juego.sokoban.columJugador] != destinoJ) {
            juego.sokoban.tablero[juego.sokoban.filaJugador]
             [juego.sokoban.columJugador] = libre;
           }
      juego.sokoban.tablero[filaDestino][columnaDestino] = destinoJ;
      break;
    case destinoC:
      if (posDos == libre) {
        moverJ = true;
        juego.sokoban.cajasOK--;
        juego.sokoban.tablero[filaDestino][columnaDestino] = destinoJ;
        juego.sokoban.tablero[filaSiguiente][columnaSiguiente] = caja;
      } else if(posDos == destinoL){
        moverJ = true;
        juego.sokoban.tablero[filaDestino][columnaDestino] = destinoJ;
        juego.sokoban.tablero[filaSiguiente][columnaSiguiente] = destinoC;
      }
      break;
  }

  if (moverJ) {
    juego.numMovimientos++;
    if (juego.sokoban.tablero[juego.sokoban.filaJugador]
        [juego.sokoban.columJugador] == destinoJ) {
          juego.sokoban.tablero[juego.sokoban.filaJugador]
          [juego.sokoban.columJugador] = destinoL;
    } else{
      juego.sokoban.tablero[juego.sokoban.filaJugador]
       [juego.sokoban.columJugador] = libre;
    }
    if(juego.sokoban.tablero[filaDestino][columnaDestino] != destinoJ){
      juego.sokoban.tablero[filaDestino][columnaDestino] = jugador;
    }
    juego.sokoban.filaJugador = filaDestino;
    juego.sokoban.columJugador = columnaDestino;
  }
}

bool bloqueado(const tJuego &juego) {
  //comprobar que dos lados de una caja "tienen" muro
  int lados = 0;
  bool bloq = false;
  for (int i = 0; i < MAX; i++) {
    for (int j = 0; j < MAX; j++) {
      if(juego.sokoban.tablero[i][j] == caja && !bloq){
        if ((juego.sokoban.tablero[i+1][j] == muro &&
             juego.sokoban.tablero[i][j+1] == muro) ||
            (juego.sokoban.tablero[i+1][j] == muro &&
             juego.sokoban.tablero[i][j-1] == muro) ||
            (juego.sokoban.tablero[i-1][j] == muro &&
             juego.sokoban.tablero[i][j-1] == muro) ||
            (juego.sokoban.tablero[i-1][j] == muro &&
             juego.sokoban.tablero[i][j+1] == muro)) {
               bloq = true;
        }
      }
    }
  }
  return bloq;
}

bool deshacerMovimiento(tJuego &juego) {
  //alamcenar un array de snapshots de tSokoban
  bool deshecho = false;
  if(juego.historia.cont > 0 && juego.numMovimientos > 0){
    juego.sokoban = juego.historia.sokoban[juego.historia.cont-1];
    juego.numMovimientos--;
    juego.historia.cont--;
    juego.deshechos++;
    deshecho = true;
  }
  return deshecho;
}
bool operator<(tPartida part1, tPartida part2) {
	bool menor = false;
	if (part1.nFichero < part2.nFichero)menor = true;
	else if (part1.nFichero == part2.nFichero) {
		if (part1.nivel < part2.nivel) menor = true;
	}
	return menor;
}

bool operator==(tPartida part1, tPartida part2) {
	return (part1.nFichero == part2.nFichero && part1.nivel == part2.nivel);
}

void guardarInfo(tPartida &partida, tInfo &info){
  bool encontrado = false, sobrepas = false;
  int i;
  for (i = 0; i < info.resueltas && (!encontrado && !sobrepas); i++) {
    if (info.exitos[i] == partida){
      encontrado = true;
    } else if (partida < info.exitos[i]){
       sobrepas = true;
     }
  }
  i--;
  if (encontrado && partida.numMovimientos < info.exitos[i].numMovimientos){
    info.exitos[i].numMovimientos = partida.numMovimientos;
  }  else if (sobrepas) {
    for (int j = info.resueltas; j > i; j--) {
      info.exitos[j] = info.exitos[j - 1];
    }
    info.exitos[i] = partida;
    info.resueltas++;
  }
  else if (i == info.resueltas - 1) {
    info.exitos[info.resueltas] = partida;
    info.resueltas++;
  }
  ofstream fichero(info.nombre);
	if (fichero.is_open()) {
		for (int i = 0; i < info.resueltas; i++) {
			fichero << info.exitos[i].nFichero << " " << info.exitos[i].nivel << " "
      << info.exitos[i].numMovimientos << "\n";
		}
		fichero.close();
	}
}

/*
"Debugging is like being the detective in a crime movie
 where you are also the murderer."

        -Filipe Fortes-
*/
