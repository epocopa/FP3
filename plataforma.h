/**
 * Esta libreria permite colorear la salida tanto bajo windows como bajo linux
 *
 * Uso:
 *   - debe estar incluida en el proyecto
 *   - debe haber un #include "colores.h" al comienzo del fichero
 *   - llama a setColor(color) para cambiar el color actual
 *      usa las constantes Gris, Rojo, Verde, Azul para simplificarte la vida!
 *   - llama a cout << algo para mostrar 'algo' en ese color
 *
 * Autor: manuel.freire@fdi.ucm.es
 * Licencia: https://creativecommons.org/licenses/by-sa/3.0/
 */

// colores validos
enum Color {
    Gris, Rojo, Verde, Azul, Blanco=15
};

#ifdef _WIN32
// este codigo solo se ejecuta bajo Windows -->

    #include <windows.h>
    #include <conio.h>

    void setColor(int color) {
        WORD a = 0;
        switch (color) {
            case Gris: a = FOREGROUND_RED|FOREGROUND_BLUE|FOREGROUND_GREEN; break;
            case Rojo: a = FOREGROUND_RED|FOREGROUND_INTENSITY; break;
            case Verde: a = FOREGROUND_GREEN|FOREGROUND_INTENSITY; break;
            case Azul: a = FOREGROUND_BLUE|FOREGROUND_INTENSITY; break;
            default: a = color;
        }
        SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), a);
    }

// <-- fin de codigo windows-exclusivo
#endif
#ifdef __linux__
// este codigo solo se ejecuta bajo linux -->

    #include <iostream>
    #include <sstream>
    #include <termios.h>
    #include <unistd.h>
    #include <stdio.h>

    enum Escape {
    FondoAzul, FondoVerde,
    BlancoSobreRojo, BlancoSobreAmarillo, BlancoSobreNegro,
    LimpiaPantalla, Reset
    };

    namespace Colores {

        // constantes globales para colores

        const char *ANSI_WHITE_ON_BLACK = "\x1b[37;40;1m";
        const char *ANSI_COLOR_GREEN_BG = "\x1b[42;1m";
        const char *ANSI_WHITE_ON_GRAY  = "\x1b[37;40;1m";
        const char *ANSI_COLOR_BLUE_BG  = "\x1b[37;44;1m";
        const char *ANSI_WHITE_ON_REDG  = "\x1b[37;41;1m";
        const char *ANSI_WHITE_ON_YELLOW= "\x1b[37;43;1m";

        const char *ANSI_COLOR_RESET  = "\x1b[0m";

        const char *ANSI_CLS  = "\x1b[2J";
    }

        void configuraConsola(Escape e) {
        switch(e) {
          case FondoAzul : std::cout << Colores::ANSI_COLOR_BLUE_BG; break;
          case FondoVerde : std::cout << Colores::ANSI_COLOR_GREEN_BG; break;
          case BlancoSobreRojo: std::cout << Colores::ANSI_WHITE_ON_REDG; break;
          case BlancoSobreAmarillo: std::cout << Colores::ANSI_WHITE_ON_YELLOW; break;
          case BlancoSobreNegro: std::cout << Colores::ANSI_WHITE_ON_BLACK; break;
          case LimpiaPantalla: std::cout << Colores::ANSI_CLS; break;
          case Reset: std::cout << Colores::ANSI_COLOR_RESET; break;
          default: std::cout << "Esto no deberia suceder"<<std::endl;
          }
         }

    // emula getch() de conio.h
    int _getch() {
        // from http://stackoverflow.com/a/23035044/15472
        struct termios oldattr, newattr;
        int ch;
        tcgetattr( STDIN_FILENO, &oldattr );
        newattr = oldattr;
        newattr.c_lflag &= ~( ICANON | ECHO );
        tcsetattr( STDIN_FILENO, TCSANOW, &newattr );
        ch = getchar();
        tcsetattr( STDIN_FILENO, TCSANOW, &oldattr );
        return ch;
    }

// <-- fin de codigo linux-exclusivo
#endif
